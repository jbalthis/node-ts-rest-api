import crypto from "crypto";
import dotenv from 'dotenv';

// load env var
dotenv.config();
const AUTH_SECRET = process.env.AUTH_SECRET;
console.log(`AUTH_SECRET => ${AUTH_SECRET}`)

export const random = () => crypto.randomBytes(128).toString('base64');
export const authentication = (salt: string, password: string) => {
    return crypto.createHmac('sha256', [salt, password].join('/')).update(AUTH_SECRET).digest('hex')
}