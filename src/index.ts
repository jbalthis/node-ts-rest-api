/*
 *  Express / Node / Mongo Server config and startup
 */

//IMPORTS
import express from 'express';
import http from "http";
import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import cors from 'cors';
import mongoose from "mongoose";
import dotenv from 'dotenv';

import router from './router';

// CONFIG
dotenv.config();

const EXPRESS_SERVER_BASE_URL = process.env.EXPRESS_SERVER_BASE_URL;
const EXPRESS_SERVER_PORT = process.env.EXPRESS_SERVER_PORT
const MONGO_DB_URL = process.env.MONGO_DB_URL;

const app = express();

// MIDDLEWARE
app.use(cors({
    credentials: true
}));
app.use(compression());
app.use(cookieParser());
app.use(bodyParser.json())

// SERVER STARTUP
const server = http.createServer(app);

server.listen(EXPRESS_SERVER_PORT, () => {
    console.log(`Express server running ${EXPRESS_SERVER_BASE_URL}:${EXPRESS_SERVER_PORT}/`)
})

mongoose.Promise = Promise;
mongoose.connect(MONGO_DB_URL);
mongoose.connection.on('error', (error: Error) => console.log(`Error connecting to Mongodb.\nERROR => ${error}`))


// ROUTES
app.use('/', router());